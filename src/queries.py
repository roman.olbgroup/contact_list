DROP_TABLE = "DROP TABLE IF EXISTS contact_list"

CREATE_TABLE = """CREATE TABLE contact_list(id SERIAL PRIMARY KEY, first_name varchar(50) NOT NULL,
                last_name varchar(50) NOT NULL, email varchar(50) UNIQUE NOT NULL)"""

INSERT_VALUES = "INSERT INTO contact_list (first_name, last_name, email) VALUES (%s, %s, %s)"

UPDATE_VALUES = """INSERT INTO contact_list (first_name, last_name, email) VALUES (%s, %s, %s)
                ON CONFLICT (email) DO UPDATE
                SET
                first_name = EXCLUDED.first_name,
                last_name = EXCLUDED.last_name;"""

ACTIVATE_TRGM_EXT = 'CREATE EXTENSION pg_trgm;'

CREATE_INDEX = ("CREATE INDEX idx ON contact_list USING gin(to_tsvector(first_name || ' ' || last_name || ' ' || "
                "email));")

FULLTEXT_SEARCH = """SELECT *
                FROM contact_list
                WHERE to_tsvector(first_name || ' ' || last_name || ' ' || email) @@ to_tsquery('{query}');"""

TEST_QUERY = "SELECT * FROM contact_list"
