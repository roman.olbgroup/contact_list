import csv

from requests import Request, Session

from src import queries
from src.services import db_pool
from settings import PATH_TO_DEFAULT_CONTACTS, FIELDS
from src.queries import ACTIVATE_TRGM_EXT, CREATE_INDEX


def cleanup_and_start():
    db_pool.execute_query(queries.DROP_TABLE)
    db_pool.execute_query(queries.CREATE_TABLE)
    with open(PATH_TO_DEFAULT_CONTACTS) as file:
        contacts = csv.reader(file, delimiter=",")
        next(contacts)
        db_pool.execute_many(queries.INSERT_VALUES, list(contacts))
    db_pool.execute_query(ACTIVATE_TRGM_EXT)
    db_pool.execute_query(CREATE_INDEX)

def get_page_result(request: Request, page=1):
    s = Session()
    request.params["page"] = page
    prepared = request.prepare()
    response = s.send(prepared)
    s.close()
    if response.status_code == 200:
        data = response.json()
        return data

def process_response(items):
    for item in items:
        values = tuple(item['fields'].get(field, [{}])[0].get('value') for field in FIELDS)
        yield values
