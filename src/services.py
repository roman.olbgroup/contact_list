import psycopg2

import settings


class DBPOOL:
    def _create_cursor(self):
        self.conn = psycopg2.connect(host=settings.DB_HOST,
                                     user=settings.DB_USER,
                                     password=settings.DB_PASSWORD)
        self.cursor = self.conn.cursor()

    def _close_conn(self):
        if self.conn:
            self.conn.commit()
            self.conn.close()

    def _get_rows(self):
        return self.cursor.fetchall()

    def _execute_query(self, query):
        self._create_cursor()
        self.cursor.execute(query)

    def execute_query(self, query):
        try:
            self._execute_query(query)
            self._close_conn()
        except psycopg2.Error:
            pass  # some error handling might be here

    def execute_many(self, query, values):
        try:
            self._create_cursor()
            self.cursor.executemany(query, values)
            self._close_conn()
        except psycopg2.Error:
            pass

    def get_rows(self, query):
        try:
            self._execute_query(query)
            rows = self._get_rows()
            self._close_conn()
            return rows
        except psycopg2.Error:
            pass


db_pool = DBPOOL()
