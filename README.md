# Contact list app

This app does following:
- initializes database table with values from csv file
- once a day makes request to https://api.nimble.com/api/v1/contacts/
and updates values in database. If there are conflicts app uses
values from response as actual and updates records
- fulltext search in existing records in database. For performing you
should make GET request to **/search** endpoint and provide param **query**
with desired value for search, and all records that match this query will be
in response.

## Prerequisites
- It's assumed that if you run this app you have installed PostgreSQL and
this service is active on default port 5432.
- Pass as environment variables credentials for access to PostgreSQL db:
DB_HOST, DB_NAME, DB_PORT (default 5432), DB_USER, DB_PASSWORD
- Pass Bearer token as env. var. TOKEN for access to API (app makes authorized request)
- It's assumed that you have installed redis server, and it runs on default 
port 6379
- Install packages in requirements.txt

## Run app
In 3 different terminals you should run following commands:

- uvicorn main:app --reload 
- celery -A celery_app worker --loglevel=info
- celery -A celery_app beat --loglevel=info

