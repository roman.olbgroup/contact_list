import os

DB_HOST = os.getenv('DB_HOST', 'localhost')
DB_NAME = os.getenv('DB_NAME', 'my_db')
DB_PORT = os.getenv('DB_PORT', '5432')
DB_USER = os.getenv('DB_USER', 'postgres')
DB_PASSWORD = os.getenv('DB_PASSWORD', 'qwerty12345')

PATH_TO_DEFAULT_CONTACTS = 'src/nimble_contacts_sheet1.csv'

REDIS_SERVER = 'redis://localhost:6379/0'

BASE_URL = 'https://api.nimble.com/api/v1/'
CONTACTS_ENDPOINT = 'contacts'
TOKEN = os.getenv('TOKEN')

FIELDS = ('first name', 'last name', 'email')
RECORD_TYPE = 'person'
