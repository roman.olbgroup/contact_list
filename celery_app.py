from concurrent.futures import ThreadPoolExecutor
from itertools import repeat

from requests import Request
from celery import Celery
from celery.schedules import crontab

from settings import BASE_URL, TOKEN, CONTACTS_ENDPOINT, FIELDS, REDIS_SERVER, RECORD_TYPE
from src.queries import UPDATE_VALUES
from src.services import db_pool
from src.utils import get_page_result, process_response

celery_app = Celery("app", broker=REDIS_SERVER)

@celery_app.task
def update_contacts_list():
    url = BASE_URL + CONTACTS_ENDPOINT
    headers = {
        'Authorization': f'Bearer {TOKEN}'
    }
    params = {
        'fields': ','.join(FIELDS),
        'record_type': RECORD_TYPE,
    }
    rows_for_update = []
    request = Request('GET', url=url, headers=headers, params=params)
    first_page_res = get_page_result(request)
    if first_page_res:
        for item in process_response(first_page_res['resources']):
            rows_for_update.append(item)
        pages = first_page_res["meta"]["pages"]
        if pages > 1:
            with ThreadPoolExecutor(max_workers=10) as executor:
                for response in executor.map(get_page_result,   repeat(request), range(2, pages+1)):
                    for item in process_response(response['resources']):
                        rows_for_update.append(item)
    if rows_for_update:
        rows_for_update = [values for values in rows_for_update if all(values)]
        db_pool.execute_many(UPDATE_VALUES, rows_for_update)


celery_app.conf.beat_schedule = {
    "update_contacts_list": {
        "task": "src.celery_app.update_contacts_list",
        "schedule": crontab(minute="0", hour="1"),  # run every day at 1:00 AM
    }
}
