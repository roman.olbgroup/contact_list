from fastapi import FastAPI
import uvicorn

from src.utils import cleanup_and_start
from src.services import db_pool
from src.queries import FULLTEXT_SEARCH, TEST_QUERY

app = FastAPI()


@app.get("/")
def root():
    return "Server running"

@app.get("/search")
def fulltext_search(query: str):
    sql_query = FULLTEXT_SEARCH.format(query=query)
    rows = db_pool.get_rows(sql_query)
    result = []
    for row in rows:
        item = {
            "id": row[0],
            "first_name": row[1],
            "last_name": row[2],
            "email": row[3]
        }
        result.append(item)
    return result


@app.on_event("startup")
def setup_db():
    cleanup_and_start()


@app.on_event("shutdown")
def shutdown_celery_app():
    app.celery_app.close()
    app.celery_app.join()


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)




